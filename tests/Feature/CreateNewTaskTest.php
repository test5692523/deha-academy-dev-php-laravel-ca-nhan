<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewTaskTest extends TestCase
{
    public function getCreateTaskRoute()
    {
        return route('tasks.store');
    }

    public function getCreateViewTaskRoute()
    {
        return route('tasks.create');
    }

    public function getListTasksRoute()
    {
        return route('tasks.index');
    }

    /** @test */
    public function authenticated_user_can_create_new_task(): void
    {
        $task = Task::factory()->make()->toArray();
        $this->actingAs(User::factory()->create());
        $response = $this->post($this->getCreateTaskRoute(), $task);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListTasksRoute());
        $this->assertDatabaseHas('tasks', $task);
    }

    /** @test */
    public function unauthenticated_user_cannot_create_new_task()
    {
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getCreateTaskRoute(), $task);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_cannot_create_task_if_data_is_null()
    {
        $task = Task::factory()->make(['content' => null, 'name' => null])->toArray();
        $this->actingAs(User::factory()->create());
        $response = $this->post($this->getCreateTaskRoute(), $task);
        $response->assertSessionHasErrors(['name', 'content']);
    }

    /** @test */
    public function authenticated_user_cannot_create_task_if_name_field_is_null()
    {
        $task = Task::factory()->make(['name' => null])->toArray();
        $this->actingAs(User::factory()->create());
        $response = $this->post($this->getCreateTaskRoute(), $task);
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function authenticated_user_cannot_create_task_if_content_field_is_null()
    {
        $task = Task::factory()->make(['content' => null])->toArray();
        $this->actingAs(User::factory()->create());
        $response = $this->post($this->getCreateTaskRoute(), $task);
        $response->assertSessionHasErrors('content');
    }

    /** @test */
    public function authenticated_user_can_see_content_error_if_validated_error()
    {
        $task = Task::factory()->make(['content' => null])->toArray();
        $this->actingAs(User::factory()->create());
        $response = $this->from($this->getCreateViewTaskRoute())->post($this->getCreateTaskRoute(), $task);
        $response->assertRedirect($this->getCreateViewTaskRoute());
        $response->assertSessionHasErrors('content');
    }

    /** @test */
    public function authenticated_user_can_see_name_error_if_validated_error()
    {
        $task = Task::factory()->make(['name' => null])->toArray();
        $this->actingAs(User::factory()->create());
        $response = $this->from($this->getCreateViewTaskRoute())->post($this->getCreateTaskRoute(), $task);
        $response->assertRedirect($this->getCreateViewTaskRoute());
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function authenticated_user_can_view_create_task_form()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getCreateViewTaskRoute());
        $response->assertViewIs('tasks.create');
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function unauthenticated_user_redirect_to_login_page()
    {
        $task = Task::factory()->make()->toArray();
        $response = $this->from($this->getCreateViewTaskRoute())->post($this->getCreateTaskRoute(), $task);
        $response->assertRedirect(route('login'));
    }
}
