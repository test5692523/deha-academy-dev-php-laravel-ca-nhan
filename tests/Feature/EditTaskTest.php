<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class EditTaskTest extends TestCase
{
    public function getEditTaskViewRoute($id)
    {
        return route('tasks.edit', $id);
    }

    public function getListTaskRoute()
    {
        return route('tasks.index');
    }

    public function getUpdateTaskRoute($id)
    {
        return route('tasks.update', $id);
    }
    /** @test */
    public function authenticated_user_can_view_edit_task_page()
    {
        $task = Task::factory()->create();
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getEditTaskViewRoute($task->id));
        $response->assertViewIs('tasks.edit');
        $response->assertViewHas('task', $task);
    }

    /** @test */
    public function unauthenticated_user_cannot_view_edit_task_page()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getEditTaskViewRoute($task->id));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_cannot_edit_task_if_task_not_exist()
    {
        $taskId = -1;
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getEditTaskViewRoute($taskId));
        $response->assertRedirect($this->getListTaskRoute());
        $response->assertSessionHas('notify', 'Task not found');
    }

    /** @test */
    public function authenticated_user_can_update_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $taskUpdate = Task::factory()->make()->toArray();
        $response = $this->put($this->getUpdateTaskRoute($task->id), $taskUpdate);
        $response->assertRedirect($this->getListTaskRoute());
        $response->assertSessionHas('success', 'Task updated successfully');
        $this->assertDatabaseHas('tasks', $taskUpdate);
    }

    /** @test */
    public function authenticated_user_cannot_update_task_if_data_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $taskUpdate = Task::factory()->make(['content' => null, 'name' => null])->toArray();
        $response = $this->put($this->getUpdateTaskRoute($task->id), $taskUpdate);
        $response->assertSessionHasErrors(['name', 'content']);
    }
}
