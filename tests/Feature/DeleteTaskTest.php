<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    public function getDeleteTaskRoute($id)
    {
        return route('tasks.destroy', $id);
    }

    public function getListTasksRoute()
    {
        return route('tasks.index');
    }
    /** @test */
    public function authenticated_user_can_delete_task(): void
    {
        $task = Task::factory()->create();
        $this->actingAs(User::factory()->create());
        $response = $this->delete($this->getDeleteTaskRoute($task->id));
        $response->assertRedirect($this->getListTasksRoute());
        $response->assertSessionHas('success', 'Task deleted successfully');
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('tasks', $task->toArray());
    }

    /** @test */
    public function unauthenticated_user_cannot_delete_task()
    {
        $task = Task::factory()->create();
        $response = $this->delete($this->getDeleteTaskRoute($task->id));
        $response->assertRedirect(route('login'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_user_cannot_delete_task_if_task_not_exists()
    {
        $taskId = -1;
        $this->actingAs(User::factory()->create());
        $response = $this->delete($this->getDeleteTaskRoute($taskId));
        $response->assertRedirect($this->getListTasksRoute());
        $response->assertSessionHas('notify', 'Task not found');
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
