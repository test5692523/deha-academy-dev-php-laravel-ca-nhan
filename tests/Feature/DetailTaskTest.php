<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DetailTaskTest extends TestCase
{

    public function getListTasksRoute()
    {
        return route('tasks.index');
    }

    public function getDetailTaskRoute($id)
    {
        return route('tasks.show', $id);
    }
    /** @test */
    public function authenticated_user_can_see_detail_task(): void
    {
        $task = Task::factory()->create();
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getDetailTaskRoute($task->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.detail');
        $response->assertViewHas('task', $task);
    }

    /** @test */
    public function unauthenticated_user_cannot_see_detail_task()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getDetailTaskRoute($task->id));
        $response->assertRedirect(route('login'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_user_cannot_see_detail_task_if_task_not_exist()
    {
        $taskId = -1;
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getDetailTaskRoute($taskId));
        $response->assertRedirect($this->getListTasksRoute());
        $response->assertSessionHas('notify', 'Task not found');
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
