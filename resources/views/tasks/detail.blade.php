@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col col-md-8">
                <h3>Edit Task</h3>
                <div class="col col-md-8">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" disabled name="name" id="name" value="{{$task->name}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea name="content" disabled id="content" cols="30" rows="10" class="form-control">{{$task->content}}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
