@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if(session('notify'))
                <div class="alert alert-danger">
                    {{session('notify')}}
                </div>
            @endif

            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            <div>
                <a href="{{route('tasks.create')}}" class="btn btn-success">Add new task</a>
            </div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Content</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                @foreach($tasks as $task)
                    <tr>
                        <td>{{$task->id}}</td>
                        <td>{{$task->name}}</td>
                        <td>{{$task->content}}</td>
                        <td>
                            <a href="{{route('tasks.show', $task->id)}}" class="btn btn-outline-success">Detail</a>
                            <a href="{{route('tasks.edit', $task->id)}}" class="btn btn-primary">Edit</a>
                            <form action="{{route('tasks.destroy', $task->id)}}" method="post">
                                @csrf
                                @method('delete')
                                <input type="submit" value="Delete" class="btn btn-danger">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
